package ioasys.joaorene.dto;

import ioasys.joaorene.enumeration.CastRoleEnumeration;
import ioasys.joaorene.enumeration.GenreEnumeration;
import lombok.Data;
import org.springframework.data.domain.Pageable;


@Data
public class FilmFilterDTO {
    Pageable pageable;
    CastRoleEnumeration castRole;
    GenreEnumeration genre;
    String filmTitle;
    String name;
}
