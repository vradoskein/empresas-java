package ioasys.joaorene.dto;

import ioasys.joaorene.enumeration.CastRoleEnumeration;
import lombok.Data;

@Data
public class PersonDTO {
    private Long id;
    private String name;
    private String age;
    private CastRoleEnumeration role;
}
