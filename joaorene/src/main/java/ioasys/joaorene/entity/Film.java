package ioasys.joaorene.entity;

import ioasys.joaorene.enumeration.GenreEnumeration;
import ioasys.joaorene.enumeration.RatingEnum;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "film")
public class Film implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "release_year")
    private String releaseYear;

    @Column(name = "genre")
    @Enumerated(EnumType.STRING)
    private GenreEnumeration genre;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "film_cast",
            joinColumns = @JoinColumn(name = "film_id"),
            inverseJoinColumns = @JoinColumn(name = "person_id")
    )
    private List<Person> person = new ArrayList<>();


   @Column(name = "votes")
    private Long votes = 0L;

    @Transient
    private Double averageVote;



    public Double getAverageVote(){
        Double average = votes != null ? votes.doubleValue() : 0D;
        return  average / RatingEnum.values().length;
    }

}