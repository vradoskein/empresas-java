package ioasys.joaorene.service.mapper;

import ioasys.joaorene.dto.ApplicationUserDTO;
import ioasys.joaorene.entity.ApplicationUser;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;

@Mapper(componentModel = "spring", uses = {RoleMapper.class}, imports = LocalDateTime.class)
public interface ApplicationUserMapper extends EntityMapper<ApplicationUserDTO, ApplicationUser> {


    @Override
    @Mapping(target = "roles" )
    @Mapping(target = "password", expression = "java(null)") // password is a sensitive info, so it is null on the dto
    ApplicationUserDTO toDto(ApplicationUser entity);

    @Override
    @Mapping(target="roles")
    ApplicationUser toEntity(ApplicationUserDTO dto);

    default ApplicationUser fromId(Long id){
        if(id == null){
            return null;
        }
        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setId(id);
        return applicationUser;
    }
}
