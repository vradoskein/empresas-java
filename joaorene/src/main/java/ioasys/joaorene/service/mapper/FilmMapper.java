package ioasys.joaorene.service.mapper;

import ioasys.joaorene.dto.FilmDTO;
import ioasys.joaorene.entity.ApplicationUser;
import ioasys.joaorene.entity.Film;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {PersonMapper.class})
public interface FilmMapper extends EntityMapper<FilmDTO, Film>  {
    @Override
    @Mapping(source = "person", target = "cast" )
    @Mapping(target="averageVote", expression = "java(entity.getAverageVote())")
    FilmDTO toDto(Film entity);

    @Override
    @Mapping(source = "cast" , target="person")
    Film toEntity(FilmDTO dto);

    default ApplicationUser fromId(Long id){
        if(id == null){
            return null;
        }
        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setId(id);
        return applicationUser;
    }
}
