package ioasys.joaorene.service.mapper;

import ioasys.joaorene.dto.RoleDTO;
import ioasys.joaorene.entity.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoleMapper extends EntityMapper<RoleDTO, Role>{
    @Override
    @Mapping(target = "id")
    @Mapping(target = "name")
    RoleDTO toDto(Role entity);

    @Override
    @Mapping(target = "id")
    @Mapping(target = "name")
    Role toEntity(RoleDTO dto);


    default Role fromId(Long id){
        if(id == null){
            return null;
        }
        Role role = new Role();
        role.setId(id);
        return role;
    }
}
