package ioasys.joaorene.service;

import ioasys.joaorene.dto.FilmDTO;
import ioasys.joaorene.dto.FilmFilterDTO;
import ioasys.joaorene.entity.Film;
import ioasys.joaorene.enumeration.CastRoleEnumeration;
import ioasys.joaorene.enumeration.GenreEnumeration;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;


public interface FilmService {

    List<Film> getFilmByPersonNameRole(String name , CastRoleEnumeration role);
    List<Film> getAllFilms(Pageable pageable);
    List<Film> getFilmByGenre(GenreEnumeration genre);
    Film getFilmByTitle(String title);
    Optional<Film> findById(Long id);

    List<FilmDTO> findFilmByFilter(FilmFilterDTO filter);

    List<FilmDTO> findAllOrderByTitleAsc();
}
