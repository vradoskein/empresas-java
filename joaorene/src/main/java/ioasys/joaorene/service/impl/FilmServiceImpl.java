package ioasys.joaorene.service.impl;

import ioasys.joaorene.dto.FilmDTO;
import ioasys.joaorene.dto.FilmFilterDTO;
import ioasys.joaorene.entity.Film;
import ioasys.joaorene.enumeration.CastRoleEnumeration;
import ioasys.joaorene.enumeration.GenreEnumeration;
import ioasys.joaorene.service.FilmService;
import ioasys.joaorene.service.facade.Facade;
import ioasys.joaorene.service.specification.FilmSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FilmServiceImpl implements FilmService {
    @Autowired
    private Facade facade;

    @Override
    public List<Film> getFilmByPersonNameRole(String title, CastRoleEnumeration role) {
        return facade.repository.film.findByTitleAndPerson(title , role);
    }

    @Override
    public List<Film> getAllFilms(Pageable pageable) {
        return new ArrayList<>(facade.repository.film.findAll(pageable).getContent());
    }




    @Override
    public Film getFilmByTitle(String title) {
        return facade.repository.film.findByTitle(title);
    }

    @Override
    public Optional<Film> findById(Long id) {
        return facade.repository.film.findById(id);
    }

    @Override
    public List<FilmDTO> findFilmByFilter(FilmFilterDTO filter) {
        Sort sort = Sort.by(
                Sort.Order.desc("votes"),
                Sort.Order.asc("title"));
        Pageable page = PageRequest.of(0,10,sort);
        if(filter.getPageable() != null){
            page = filter.getPageable();
        }
        Page<Film> film = facade.repository.film.findAll(new FilmSpecification(filter), page);
        return facade.mapperStruct.film.toDto(new ArrayList<>(film.getContent()));
    }

    @Override
    public List<FilmDTO> findAllOrderByTitleAsc() {
        return facade.mapperStruct.film.toDto(facade.repository.film.findAllByOrderByTitleAsc());
    }

    @Override
    public List<Film> getFilmByGenre(GenreEnumeration genre) {
        return facade.repository.film.findByGenre(genre);
    }
}
