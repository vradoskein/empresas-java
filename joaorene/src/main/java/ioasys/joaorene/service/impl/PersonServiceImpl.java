package ioasys.joaorene.service.impl;

import ioasys.joaorene.entity.Person;
import ioasys.joaorene.service.PersonService;
import ioasys.joaorene.service.facade.Facade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private Facade facade;

    @Override
    public List<Person> getAllActors() {
        return facade.repository.person.findAll();
    }
}
