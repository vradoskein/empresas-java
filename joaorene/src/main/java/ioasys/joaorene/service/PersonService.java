package ioasys.joaorene.service;

import ioasys.joaorene.entity.Person;

import java.util.List;


public interface PersonService {

    List<Person> getAllActors();

}
