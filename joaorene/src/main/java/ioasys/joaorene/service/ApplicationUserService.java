package ioasys.joaorene.service;

import ioasys.joaorene.dto.ApplicationUserDTO;
import ioasys.joaorene.entity.ApplicationUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;


public interface ApplicationUserService {

    Set<ApplicationUser> getAllUsers();
    Page<ApplicationUser> getActiveNonAdminUsers(Pageable pageable);

    ApplicationUser updateUser(ApplicationUser userDB, ApplicationUserDTO dto, Boolean hasAdminAuthority);

    ApplicationUser deleteUser(ApplicationUser userDB, Boolean hasAdminAuthority);

    ApplicationUser updateAdmin(ApplicationUser userDB, ApplicationUserDTO dto);

    ApplicationUser deleteAdmin(ApplicationUser userDB);
}
