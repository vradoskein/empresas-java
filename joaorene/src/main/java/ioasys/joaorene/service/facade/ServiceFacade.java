package ioasys.joaorene.service.facade;

import ioasys.joaorene.service.ApplicationUserService;
import ioasys.joaorene.service.FilmService;
import ioasys.joaorene.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceFacade {
    @Autowired
    public ApplicationUserService user;

    @Autowired
    public FilmService film;

    @Autowired
    public PersonService person;


}
