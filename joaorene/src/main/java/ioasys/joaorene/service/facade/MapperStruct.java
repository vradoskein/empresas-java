package ioasys.joaorene.service.facade;

import ioasys.joaorene.service.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MapperStruct {

    @Autowired
    public ApplicationUserMapper user;

    @Autowired
    public RoleMapper role;

    @Autowired
    public PersonMapper person;

    @Autowired
    public FilmMapper film;


}
