CREATE TABLE film(
    id bigint(20) NOT NULL AUTO_INCREMENT,
    title varchar(100) NOT NULL,
    release_year  varchar(4) NOT NULL,
    genre varchar(80) NOT NULL,
    votes bigint(20) DEFAULT 0,
    UNIQUE KEY uk_title (title),
    PRIMARY KEY(id)
 );