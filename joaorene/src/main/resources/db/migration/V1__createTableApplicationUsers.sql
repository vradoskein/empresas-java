CREATE TABLE application_user(
    id bigint(20) NOT NULL AUTO_INCREMENT,
    active tinyint DEFAULT 1,
    creation_date_time DATE NOT NULL,
    username varchar(100) NOT NULL,
    password varchar(64) NOT NULL,
    name varchar(50) NOT NULL,
    email varchar(100) NOT NULL,
    PRIMARY KEY(id),
    UNIQUE KEY uk_username (username),
    UNIQUE KEY uk_email (email)
);